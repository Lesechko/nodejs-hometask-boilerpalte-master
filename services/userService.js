const {UserRepository} = require('../repositories/userRepository');

class UserService {
	getAllUsers(ob) {
		const users = UserRepository.getAll(ob);
		if (users.length === 0) {
			return null;
		}
		return users;
	}

	getUser(user) {
		return this.search(user);
	}

	createUser(body) {
		const newUser = UserRepository.create(body);
		if (!newUser) {
			return null;
		}

		return newUser;
	}

	updateUser(id, changes) {
		const updation = UserRepository.update(id, changes);
		if (!updation) {
			return null;
		}

		return updation;
	}

	deleteUser(id) {
		const del = UserRepository.delete(id);
		if (del.length === 0) {
			return null;
		}
		return del;
	}

	// TODO: Implement methods to work with user

	search(search) {
		const item = UserRepository.getOne(search);
		if (!item) {
			return null;
		}
		return item;
	}
}

module.exports = new UserService();
