const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {
	getAllFighters() {
		const fighters = FighterRepository.getAll(object);
		if (fighters.length === 0) {
			return null;
		}

		return fighters;
	}

	search(search) {
		const item = FighterRepository.getOne(search);
		if (!item) {
			return null;
		}
		return item;
	}

	createFighter(body) {
		const newFighter = FighterRepository.create(body);
		if (!newFighter) {
			return null;
		}

		return newFighter;
	}

	updateFighter(id, changes) {
		const updation = FighterRepository.update(id, changes);
		if (!updation) {
			return null;
		}

		return updation;
	}

	deleteFighter(id) {
		const deletion = FighterRepository.delete(id);
		if (deletion.length === 0) {
			return null;
		}

		return deletion;
	}
}

module.exports = new FighterService();
