const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post(
	'/login',
	(req, res, next) => {
		try {
			if (req && req.headers && req.headers.authorization) {
				res.data = data;
			}
			// TODO: Implement login action
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	},
	responseMiddleware
);

module.exports = router;
