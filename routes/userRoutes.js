const {Router} = require('express');
const UserService = require('../services/userService');
const {
	createUserValid,
	updateUserValid,
} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

const {
	getUser,
	getAllUsers,
	createUser,
	updateUser,
	deleteUser,
} = require('../services/userService');

// TODO: Implement route controllers for user

router.get(
	'/',
	(req, res, next) => {
		const allUsers = getAllUsers('names');
		if (!allUsers) {
			res.status(200).json({messsage: 'No any users...'});
		} else {
			let obj = [];
			listOfUsers.forEach((elem) => {
				let {id, password, ...temp} = elem;
				obj.push(temp);
			});

			req.body = obj;
			next();
		}
	},
	(req, res) => {
		res.status(200).json({'All users': req.body});
	}
);

router.get(
	'/:id',
	(req, res, next) => {
		const user = getUser({id: req.params.id});
		if (!user) {
			res.status(404).json({
				error: true,
				messsage: 'User is not found',
			});
		} else {
			const {id, password, ...obj} = user;
			req.body = obj;
			next();
		}
	},
	(req, res) => {
		res.status(200).json({'User found': req.body});
	}
);

router.post(
	'/',
	createUserValid,
	(req, res, next) => {
		const createOfUser = createUser(req.body);
		if (!createOfUser) {
			res.status(400).json({
				error: true,
				messsage: 'Failed to create user',
			});
		} else {
			const {id, password, ...temp} = createOfUser;
			req.body = temp;
			next();
		}
	},
	(req, res) => {
		res.status(200).json({'User created': req.body});
	}
);

outer.put(
	'/:id',
	(req, res, next) => {
		if (getUser({id: req.params.id})) {
			res.status(404).json({
				error: true,
				messsage: 'User is not found',
			});
		} else {
			next();
		}
	},
	updateUserValid,
	(req, res, next) => {
		const updateOfUser = updateUser(req.params.id, req.body);
		if (!updateOfUser) {
			res.status(400).json({
				error: true,
				messsage: 'User is not updated',
			});
		} else {
			const {id, password, ...temp} = updateOfUser;
			req.body = temp;
			next();
		}
	},
	(req, res) => {
		res.status(200).json({'User updated': req.body});
	}
);

router.delete(
	'/:id',
	(req, res, next) => {
		const deleteOfUser = deleteUser(req.params.id);
		if (!deleteOfUser) {
			res.status(404).json({
				error: true,
				messsage: 'User is not found',
			});
		} else {
			const {id, password, ...temp} = deleteOfUser;
			req.body = temp;
			next();
		}
	},
	(req, res) => {
		res.status(200).json({'User deleted': req.body});
	}
);

module.exports = router;
